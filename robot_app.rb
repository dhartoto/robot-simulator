models = File.expand_path("../models", __FILE__)
lib = File.expand_path("../lib", __FILE__)
$:.unshift(models, lib)

require 'robot_simulator'
require 'command_generator'

filename = ARGV[0]

begin
  commands = CommandGenerator.run(filename)
  simulator = RobotSimulator.new(commands)
  simulator.run
rescue Simulator::ArgumentError, Simulator::FileNotFound,
  Simulator::FileEmpty => error

  puts error.message
end
