require 'errors'

class CommandValidator
  class << self
    def validate_first_command(options={})
      robot = options[:robot]
      command = options[:command]

      return if command.include?('PLACE') || on_tabletop?(robot)

      raise Simulator::InvalidCommandError, first_command_error_message
    end

    def validate_location_and_orientation(response)
      validate_location_placement(response)
      validate_orientation(response)
    end

    private

    def on_tabletop?(robot)
      robot.x_axis && robot.y_axis && robot.orientation
    end

    def validate_location_placement(response)
      return if location_valid?(response)

      raise Simulator::InvalidCommandError, invalid_location_error
    end

    def validate_orientation(response)
      return if %(NORTH SOUTH EAST WEST).include?(response.last)

      raise Simulator::InvalidCommandError, invalid_orientation_error
    end

    def location_valid?(response)
      x_axis, y_axis = response

      within_tabletop?(x_axis) && within_tabletop?(y_axis)
    end

    def within_tabletop?(axis)
      axis >= 0 && axis < 5
    end

    def invalid_location_error
      "Error: Invalid Location - x and y axis must be between 0 to 4 (inclusive)"
    end

    def first_command_error_message
      "Error: Invalid Command - First command must be 'PLACE'"
    end

    def invalid_orientation_error
      "Error: Invalid Orientation - Options 'NORTH', 'SOUTH', 'EAST', 'WEST'"
    end
  end
end
