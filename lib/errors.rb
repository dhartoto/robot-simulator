module Simulator
  class InvalidCommandError < StandardError
  end

  class FileNotFound < StandardError
  end

  class ArgumentError < ArgumentError
  end

  class FileEmpty < ArgumentError
  end
end
