require 'file_validator'

class CommandGenerator

  class << self
    def run(file)
      validate_file(file)

      command = sanatize_command(File.readlines(file))
    end

    private

    def validate_file(file)
      FileValidator.run(file)
    end

    def sanatize_command(file)
      file.map(&:strip)
    end
  end
end
