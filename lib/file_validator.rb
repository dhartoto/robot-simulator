require 'errors'

class FileValidator
  class << self
    def run(file)
      validate_file_argument_present(file)
      validate_file_present(file)
      validate_empty_file(file)
    end

    private

    def validate_file_argument_present(file)
      return unless file.nil?

      raise Simulator::ArgumentError, missing_file_argument_message
    end

    def missing_file_argument_message
      <<~eos
        Error: File required.
        Example: 'ruby robot_app.rb spec/test_files/valid_commands.txt'
      eos
    end

    def validate_file_present(file)
      return if File.exist?(file)

      raise Simulator::FileNotFound, file_not_found_message(file)
    end

    def file_not_found_message(file)
      "Error: File not found - '#{file}' missing"
    end

    def validate_empty_file(file)
      return unless File.zero?(file)

      raise Simulator::FileEmpty, file_empty_message
    end

    def file_empty_message
      <<~eos
        Error: File Empty
        To move the robot on the tabletop you need to start with a 'PLACE'
        command. The 'PLACE' command requires location (x-axis, y-axis) and
        orientation ('NORTH', 'SOUTH', 'EAST', 'WEST') arguments.
        x-axis and y-axis have to be from 0 to 4 (inclusive). Example of
        a valid 'PLACE command' is 'PLACE 1,2,SOUTH'.
        Valid commands are listed below. :
        PLACE <x-axis>,<y-axis>,<orientation>
        MOVE
        LEFT
        RIGHT
        REPORT

        Each command must be on a new line.
      eos
    end

  end
end
