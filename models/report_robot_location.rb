class ReportRobotLocation
  class << self
    def run(options={})
      robot = options[:robot]

      output_location_and_orientation(robot)
      
      build_response(robot)
    end

  private

    def output_location_and_orientation(robot)
      puts "Current Location: #{robot.x_axis},#{robot.y_axis},#{robot.orientation}"
    end

    def build_response(robot)
      {
        x_axis: robot.x_axis,
        y_axis: robot.y_axis,
        orientation: robot.orientation
      }
    end
  end
end
