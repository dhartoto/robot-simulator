require 'robot'

class RobotSimulator
  attr_accessor :robot

  def initialize(commands)
    @commands = commands
    @robot = Robot.new
  end

  def run
    begin
      commands.each do |command|
        robot.perform(command)
      end
    rescue Simulator::InvalidCommandError => error
      puts error.message
    end
  end

  private

  attr_reader :commands

end
