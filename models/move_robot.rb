class MoveRobot
  class << self
    def run(options={})
      robot = options[:robot]
      orientation = robot.orientation.downcase

      x_axis, y_axis = send("move_from_#{orientation}", robot)

      { x_axis: x_axis, y_axis: y_axis, orientation: robot.orientation }
    end

    private

    def move_from_north(robot)
      next_axis = increment_axis(robot.y_axis)

      y_axis = within_tabletop_range?(next_axis) ? next_axis : robot.y_axis

      [robot.x_axis, y_axis]
    end

    def move_from_south(robot)
      next_axis = decrement_axis(robot.x_axis)

      x_axis = within_tabletop_range?(next_axis) ? next_axis : robot.x_axis

      [x_axis, robot.y_axis]
    end

    def move_from_east(robot)
      next_axis = increment_axis(robot.x_axis)

      x_axis = within_tabletop_range?(next_axis) ? next_axis : robot.x_axis

      [x_axis, robot.y_axis]
    end

    def move_from_west(robot)
      next_axis = decrement_axis(robot.y_axis)

      y_axis = within_tabletop_range?(next_axis) ? next_axis : robot.y_axis

      [robot.x_axis, y_axis]
    end

    def increment_axis(axis)
      axis += 1
    end

    def decrement_axis(axis)
      axis -= 1
    end

    def within_tabletop_range?(axis)
      axis >= 0 && axis < 5
    end
  end
end
