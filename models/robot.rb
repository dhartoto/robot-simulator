require 'place_robot'
require 'move_robot'
require 'rotate_robot_left'
require 'rotate_robot_right'
require 'report_robot_location'
require 'command_validator'

class Robot
  attr_accessor :x_axis, :y_axis, :orientation

  def perform(command)
    validate_robot_on_tabletop(command)

    command, options = split_command_and_options(command)

    response = COMMAND_SERVICES[command].run(robot: self, options: options)

    update_position(response)
  end

  private

  COMMAND_SERVICES = {
    'PLACE' => PlaceRobot,
    'MOVE' => MoveRobot,
    'LEFT' => RotateRobotLeft,
    'RIGHT' => RotateRobotRight,
    'REPORT' => ReportRobotLocation
  }.freeze

  def validate_robot_on_tabletop(command)
    CommandValidator.validate_first_command(robot: self, command: command)
  end

  def split_command_and_options(command)
    command.split(' ')
  end

  def update_position(response)
    self.x_axis = response[:x_axis]
    self.y_axis = response[:y_axis]
    self.orientation = response[:orientation]
  end
end
