class RotateRobotRight
  class << self
    def run(options={})
      robot = options[:robot]
      {
        x_axis: robot.x_axis,
        y_axis: robot.y_axis,
        orientation: next_orientation(robot.orientation)
      }
    end

    private

    RIGHT_MOVES = {
      'NORTH' => 'EAST',
      'SOUTH' => 'WEST',
      'EAST' => 'SOUTH',
      'WEST' => 'NORTH'
    }

    def next_orientation(orientation)
      RIGHT_MOVES[orientation]
    end
  end
end
