class RotateRobotLeft
  class << self
    def run(options={})
      robot = options[:robot]
      {
        x_axis: robot.x_axis,
        y_axis: robot.y_axis,
        orientation: next_orientation(robot.orientation)
      }
    end

    private

    LEFT_MOVES = {
      'NORTH' => 'WEST',
      'SOUTH' => 'EAST',
      'EAST' => 'NORTH',
      'WEST' => 'SOUTH'
    }

    def next_orientation(orientation)
      LEFT_MOVES[orientation]
    end
  end
end
