require 'errors'
require 'command_validator'

class PlaceRobot
  class << self
    def run(options={})
      robot = options[:robot]
      args = options[:options]

      build_hash_response(args)
    end

    private

    def build_hash_response(args)
      args_array = split_arguments(args)
      validate_location_and_orientation(args_array)
      {
        x_axis: args_array[0],
        y_axis: args_array[1],
        orientation: args_array[2]
      }
    end

    def split_arguments(args)
      args.split(",").map do |arg|
        arg.match(/\d/) ? arg.to_i : arg
      end
    end

    def validate_location_and_orientation(args_array)
      CommandValidator.validate_location_and_orientation(args_array)
    end
  end
end
