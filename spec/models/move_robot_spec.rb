require 'spec_helper'
require 'move_robot'

describe MoveRobot do
  describe '.run' do
    let(:robot_double) do
      instance_double(
        'Robot',
        x_axis: 0,
        y_axis: 0,
        orientation: orientation
        )
    end

    context 'when move is valid' do
      let(:orientation) { 'NORTH' }
      let(:expected_output) do
        {
          x_axis: 0,
          y_axis: 1,
          orientation: 'NORTH'
        }
      end

      it "returns [0, 1, 'NORTH'] if position was 0,0,NORTH" do
        expect(described_class.run(robot: robot_double))
          .to eq(expected_output)
      end
    end

    context 'when move is invalid' do
      let(:orientation) { 'SOUTH' }
      let(:expected_output) do
        {
          x_axis: 0,
          y_axis: 0,
          orientation: 'SOUTH'
        }
      end

      it "returns [0, 0, 'SOUTH'] if position was 0,0,SOUTH" do
        expect(described_class.run(robot: robot_double))
          .to eq(expected_output)
      end
    end
  end
end
