require 'spec_helper'
require 'place_robot'

describe PlaceRobot do
  describe '.run' do
    let(:robot_double) do
      instance_double(
        'Robot',
        x_axis: nil,
        y_axis: nil,
        orientation: nil
        )
    end

    let(:place_options) { '0,1,SOUTH' }
    let(:expected_output) do
      {
        x_axis: 0,
        y_axis: 1,
        orientation: 'SOUTH'
      }
    end

    context 'when command is valid:' do
      context "'PLACE 0,1,SOUTH'" do

        it "returns a hash response" do
          expect(described_class.run(robot: robot_double, options: place_options))
            .to eq(expected_output)
        end
      end
    end
  end
end
