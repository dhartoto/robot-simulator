require 'spec_helper'
require 'rotate_robot_right'

describe RotateRobotRight do
  describe '.run' do
    let(:service) { described_class.run(robot) }
    let(:robot_double) do
      instance_double(
        'Robot',
        x_axis: 0,
        y_axis: 0,
        orientation: orientation
        )
    end

    context "when position is  0,0,NORTH" do
      let(:orientation) { 'NORTH' }
      let(:expected_output) do
        {
          x_axis: 0,
          y_axis: 0,
          orientation: 'EAST'
        }
      end

      it "returns hash response" do
        expect(described_class.run(robot: robot_double))
          .to eq(expected_output)
      end
    end

    context "when position is  0,0,SOUTH" do
      let(:orientation) { 'SOUTH' }
      let(:expected_output) do
        {
          x_axis: 0,
          y_axis: 0,
          orientation: 'WEST'
        }
      end

      it "returns hash response" do
        expect(described_class.run(robot: robot_double))
          .to eq(expected_output)
      end
    end
  end
end
