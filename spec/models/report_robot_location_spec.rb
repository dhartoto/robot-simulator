require 'report_robot_location'

describe ReportRobotLocation do
  context 'when robot location is x_axis: 0, x_axis 0, orientation: NORTH' do
    let(:robot_double) do
      instance_double(
        'Robot',
        x_axis: 0,
        y_axis: 0,
        orientation: 'NORTH'
        )
    end

    it 'outputs Current Location: 0,0,NORTH' do
      expect { described_class.run(robot: robot_double) }
        .to output("Current Location: 0,0,NORTH\n").to_stdout
    end
  end
end
