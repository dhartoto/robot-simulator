require 'spec_helper'
require 'robot_simulator'

describe 'Robot simulator integration test' do
  let(:simulator) { RobotSimulator.new(commands) }

  context 'when robot is not given PLACE as the first command' do
    let(:commands) { ['MOVE', 'LEFT', 'MOVE'] }

    it 'responds with invalid command error' do
      message = "Error: Invalid Command - First command must be 'PLACE'\n"

      expect { simulator.run }
        .to output(message).to_stdout
    end
  end

  context 'when robot is placed on an invalid location' do
    let(:commands) { ['PLACE 5,5,SOUTH'] }

    it 'responds with invalid location error' do
      message = "Error: Invalid Location - x and y axis must be between 0 to 4 (inclusive)\n"
      expect { simulator.run }.to output(message).to_stdout
    end
  end

  context 'when robot is given invalid orientation' do
    let(:commands) { ['PLACE 0,0,GIBBERISH'] }

    it 'responds with invalid location error' do
      message = "Error: Invalid Orientation - Options 'NORTH', 'SOUTH', 'EAST', 'WEST'\n"

      expect { simulator.run }.to output(message).to_stdout
    end
  end

  context 'when PLACE command is valid' do
    let(:commands) { ['PLACE 0,0,SOUTH', 'REPORT'] }

    it 'should place robot on the tabletop' do
      expect { simulator.run }
        .to output("Current Location: 0,0,SOUTH\n").to_stdout
    end
  end

  context 'when robot given erroneous commands after first valid command' do

    context 'when the next PLACE command is on an invalid location' do
      let(:commands) { ['PLACE 0,0,NORTH', 'PLACE 5,5,SOUTH'] }

      it 'responds with invalid location error' do
        message = "Error: Invalid Location - x and y axis must be between 0 to 4 (inclusive)\n"

        expect { simulator.run }.to output(message).to_stdout
      end
    end

    context 'when valid commands are given subsequent to an invalid command' do
      let(:commands) { ['PLACE 0,0,SOUTH', 'MOVE', 'LEFT', 'MOVE', 'REPORT'] }

      it 'ignores invalid command and performs subsequent correct commands' do
        expect { simulator.run }
          .to output("Current Location: 1,0,EAST\n").to_stdout
      end
    end

    context 'when given valid commands' do
      context 'PLACE 0,0,NORTH MOVE REPORT' do
        let(:commands) { ['PLACE 0,0,NORTH', 'MOVE', 'REPORT'] }

        it 'outputs: 0,1,NORTH' do
          expect { simulator.run }
            .to output("Current Location: 0,1,NORTH\n").to_stdout
        end
      end

      context 'PLACE 0,0,NORTH MOVE RIGHT REPORT' do
        let(:commands) { ['PLACE 0,0,NORTH', 'MOVE', 'RIGHT', 'REPORT'] }

        it 'outputs: 0,1,NORTH' do
          expect { simulator.run }
            .to output("Current Location: 0,1,EAST\n").to_stdout
        end
      end

      context 'PLACE 0,0,NORTH LEFT REPORT' do
        let(:commands) { ['PLACE 0,0,NORTH', 'LEFT', 'REPORT'] }

        it 'outputs: 0,0,WEST' do
          expect { simulator.run }
            .to output("Current Location: 0,0,WEST\n").to_stdout
        end
      end

      context 'PLACE 1,2,EAST MOVE MOVE LEFT MOVE REPORT' do
        let(:commands) do
          ['PLACE 1,2,EAST', 'MOVE', 'MOVE', 'LEFT', 'MOVE', 'REPORT']
        end

        it 'outputs: 3,3,NORTH' do
          expect { simulator.run }
            .to output("Current Location: 3,3,NORTH\n").to_stdout
        end
      end
    end
  end
end
