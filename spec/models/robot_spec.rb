require 'spec_helper'
require 'robot'

describe Robot do
  describe '.new' do
    it 'returns an instance of Robot' do
      expect(subject).to be_an_instance_of(Robot)
    end

    it 'x_axis is nil' do
      expect(subject.x_axis).to be_nil
    end

    it 'y_axis is nil' do
      expect(subject.y_axis).to be_nil
    end

    it 'orientation is nil' do
      expect(subject.orientation).to be_nil
    end
  end

  describe '#perform' do
    let(:perform_command) { subject.perform(command) }

    context 'when command is PLACE' do
      let(:command) { 'PLACE 0,1,SOUTH' }
      let(:service_response) do
        {
          x_axis: 0,
          y_axis: 1,
          orientation: 'SOUTH'
        }
      end

      before { allow(PlaceRobot).to receive(:run).and_return(service_response) }

      it 'sets robot at 0 along the x axis' do
        perform_command

        expect(subject.x_axis).to eq(0)
      end

      it 'sets robot at 1 along the y axis' do
        perform_command

        expect(subject.y_axis).to eq(1)
      end

      it 'sets robot orientation to SOUTH' do
        perform_command

        expect(subject.orientation).to eq('SOUTH')
      end
    end

    context 'when command is MOVE and robot is orientation NORTH' do
      let(:command) { 'MOVE' }
      let(:service_response) do
        {
          x_axis: 0,
          y_axis: 1,
          orientation: 'NORTH'
        }
      end

      before do
        subject.x_axis = 0
        subject.y_axis = 0
        subject.orientation = 'NORTH'
        allow(MoveRobot).to receive(:run).and_return(service_response)
      end

      it 'does not move along the x axis' do
        expect { perform_command }.not_to change(subject, :x_axis)
      end

      it 'moves forward along the y axis' do
        expect { perform_command }.to change(subject, :y_axis).by(1)
      end

      it 'does not rotate' do
        expect { perform_command }.not_to change(subject, :orientation)
      end
    end

    context 'when command is RIGHT and robot is orientation NORTH' do
      let(:command) { 'RIGHT' }
      let(:service_response) do
        {
          x_axis: 0,
          y_axis: 0,
          orientation: 'EAST'
        }
      end

      before do
        subject.x_axis = 0
        subject.y_axis = 0
        subject.orientation = 'NORTH'
        allow(RotateRobotRight).to receive(:run).and_return(service_response)
      end

      it 'does not move along the x axis' do
        expect { perform_command }.not_to change(subject, :x_axis)
      end

      it 'does not move along the y axis' do
        expect { perform_command }.not_to change(subject, :y_axis)
      end

      it 'rotates rigt to face EAST' do
        perform_command

        expect(subject.orientation).to eq('EAST')
      end
    end

    context 'when command is LEFT and robot is orientation NORTH' do
      let(:command) { 'LEFT' }
      let(:service_response) do
        {
          x_axis: 0,
          y_axis: 0,
          orientation: 'WEST'
        }
      end

      before do
        subject.x_axis = 0
        subject.y_axis = 0
        subject.orientation = 'NORTH'
        allow(RotateRobotLeft).to receive(:run).and_return(service_response)
      end

      it 'does not move along the x axis' do
        expect { perform_command }.not_to change(subject, :x_axis)
      end

      it 'does not move along the y axis' do
        expect { perform_command }.not_to change(subject, :y_axis)
      end

      it 'rotates left to face WEST' do
        perform_command

        expect(subject.orientation).to eq('WEST')
      end
    end

    context 'when command is REPORT' do
      let(:command) { 'REPORT' }

      before do
        subject.x_axis = 0
        subject.y_axis = 0
        subject.orientation = 'NORTH'
      end

      it "outputs position and orientation" do
        expect { perform_command }
          .to output("Current Location: 0,0,NORTH\n").to_stdout
      end
    end
  end
end
