require 'spec_helper'
require 'command_validator'

describe CommandValidator do
  describe '.validate_first_command' do
    let(:robot_double) do
      instance_double('Robot',
        x_axis: nil,
        y_axis: nil,
        orientation: nil
      )
    end

    context 'when robot has not been placed on the tabletop' do
      let(:command) { 'MOVE' }
      let(:validate_first_command) do
        described_class.validate_first_command(robot: robot_double, command: command)
      end

      it 'raises a Simulator::CommandError' do
        expect { validate_first_command }.to raise_error(
          Simulator::InvalidCommandError,
          "Error: Invalid Command - First command must be 'PLACE'"
          )
      end
    end
  end

  describe '.validate_location_and_orientation' do
    let(:validate_location_and_orientation) do
      described_class.validate_location_and_orientation(response)
    end

    context 'when invalid orientation' do
      let(:response) { [5, 5, 'SOUTH'] }

      it 'raises a Simulator::CommandError' do
        expect { validate_location_and_orientation }.to raise_error(
          Simulator::InvalidCommandError,
          "Error: Invalid Location - x and y axis must be between 0 to 4 (inclusive)"
          )
      end
    end

    context 'when invalid location' do
      let(:response) { [0, 1, 'GIBBERISH'] }

      it 'raises a Simulator::CommandError' do
        expect { validate_location_and_orientation }.to raise_error(
          Simulator::InvalidCommandError,
          "Error: Invalid Orientation - Options 'NORTH', 'SOUTH', 'EAST', 'WEST'"
          )
      end
    end
  end
end
