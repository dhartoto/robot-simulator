require 'spec_helper'
require 'command_generator'

describe CommandGenerator do
  describe '.run' do
    let(:filename) { 'spec/test_files/valid_commands.txt' }
    let(:generate_command) { described_class.run(filename) }

    it 'validates file' do
      expect(FileValidator).to receive(:run).with(filename)

      generate_command
    end

    it 'returns a valid command' do
      command = generate_command

      expect(command).to eq(['PLACE 0,0,NORTH', 'MOVE', 'REPORT'])
    end
  end
end
