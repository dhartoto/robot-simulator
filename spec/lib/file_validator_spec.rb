require 'spec_helper'
require 'file_validator'

describe FileValidator do
  describe '.run' do
    let(:validate_file) { described_class.run(filename) }

    context 'when filename not given' do
      let(:filename) { nil }

      it 'raises an argument error' do
        error_message = <<~eos
          Error: File required.
          Example: 'ruby robot_app.rb spec/test_files/valid_commands.txt'
        eos

        expect { validate_file }.to raise_error(
          Simulator::ArgumentError,
          error_message
        )
      end
    end

    context 'when file not found' do
      let(:filename) { 'no/file/here.txt' }

      it 'raises a file not found error' do
        error_message = "Error: File not found - '#{filename}' missing"

        expect { validate_file }.to raise_error(
          Simulator::FileNotFound,
          error_message
        )
      end
    end

    context 'when file is empty' do
      let(:filename) { 'spec/test_files/empty_file.txt' }

      it 'raises a file empty error' do
        error_message = <<~eos
          Error: File Empty
          To move the robot on the tabletop you need to start with a 'PLACE'
          command. The 'PLACE' command requires location (x-axis, y-axis) and
          orientation ('NORTH', 'SOUTH', 'EAST', 'WEST') arguments.
          x-axis and y-axis have to be from 0 to 4 (inclusive). Example of
          a valid 'PLACE command' is 'PLACE 1,2,SOUTH'.
          Valid commands are listed below. :
          PLACE <x-axis>,<y-axis>,<orientation>
          MOVE
          LEFT
          RIGHT
          REPORT

          Each command must be on a new line.
        eos

        expect { validate_file }.to raise_error(
          Simulator::FileEmpty,
          error_message
        )
      end
    end
  end
end
